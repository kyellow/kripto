/*
 * Copyright (C) 2013 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package Vigenere;

import java.util.Locale;
import java.util.Scanner;

/**
 * Türk alfabesi ile vigenere şifreleme uygulaması
 *
 * @author kyellow
 */
public class VigenereCipher {

    static final char[] alfabe = {'a', 'b', 'c', 'ç', 'd', 'e', 'f', 'g', 'ğ', 'h', 'ı', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'ö', 'p', 'r', 's', 'ş', 't', 'u', 'ü', 'v', 'y', 'z'};
    private String key;

    int keyIndex[];

    public VigenereCipher(String anahtar) {

        if (anahtar.isEmpty()) {
            key = "test";
        } else {
            StringBuilder sb = new StringBuilder(anahtar.length());

            for (int i = 0; i < anahtar.length(); i++) {

                if (Character.isLetter(anahtar.charAt(i))) {

                    for (int j = 0; j < alfabe.length; j++) {

                        if (alfabe[j] == anahtar.charAt(i)) {
                            sb.append(anahtar.charAt(i));
                        }
                    }
                }
            }

            key = sb.toString();
        }

        this.initKeyIndex();
    }

    public void initKeyIndex() {
        keyIndex = new int[key.length()];

        for (int i = 0; i < key.length(); i++) {
            for (int j = 0; j < alfabe.length; j++) {

                if (alfabe[j] == key.charAt(i)) {
                    keyIndex[i] = j;
                }
            }
        }
    }

    /**
     * E(M(x)) = (M(x) + b) mod m
     */
    public String crypt(String ctxt) {

        if (ctxt.isEmpty()) {
            return ctxt;
        }

        char[] clist = ctxt.toLowerCase(new Locale("tr")).toCharArray();
        int index = 0;

        for (int i = 0; i < clist.length; i++) {

            if (Character.isLetter(clist[i])) {

                for (int j = 0; j < alfabe.length; j++) {

                    if (clist[i] == alfabe[j]) {
                        index = keyIndex[(i % key.length())];
                        clist[i] = alfabe[(j + index) % 29];
                        if (clist[i] == 'i') {
                            clist[i] = 'İ';
                        } else {
                            clist[i] = Character.toUpperCase(clist[i]);
                        }
                        break;
                    }
                }
            }
        }

        return new String(clist);
    }

    /**
     * D(C(x)) = (C(x) - b) mod m
     */
    public String decrypt(String stxt) {

        if (stxt.isEmpty()) {
            return stxt;
        }

        char[] clist = stxt.toLowerCase(new Locale("tr")).toCharArray();
        int index = 0;

        for (int i = 0; i < clist.length; i++) {

            if (Character.isLetter(clist[i])) {

                for (int j = 0; j < alfabe.length; j++) {

                    if (clist[i] == alfabe[j]) {
                        index = keyIndex[(i % key.length())];
                        clist[i] = alfabe[(j - index + 29) % 29];
                        if (clist[i] == 'i') {
                            clist[i] = 'İ';
                        } else {
                            clist[i] = Character.toUpperCase(clist[i]);
                        }
                        break;
                    }
                }
            }
        }

        return new String(clist);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String anahtar = "sarı";
        String alist = "abcçdefgğhıijklmnoöprsştuüvyz";
        System.out.println(" Alfabe : " + alist);
        VigenereCipher c = new VigenereCipher(anahtar);
        System.out.println(" Anahtar : " + anahtar + " , Alfabe : " + c.crypt(alist));

        Scanner scan = new Scanner(System.in);
        System.out.print("Text bir Anahtar Girin: ");
        anahtar = scan.nextLine();

        VigenereCipher cc = new VigenereCipher(anahtar);
        System.out.println("Mesaj Girin : ");
        String message = scan.nextLine();
        System.out.println("Açık Metin : " + message);

        String sifreliMetin = cc.crypt(message);
        System.out.println("Şifrelenmiş Metin " + sifreliMetin);
        System.out.println("Deşifrelenmiş Metin : " + cc.decrypt(sifreliMetin));

    }
}
