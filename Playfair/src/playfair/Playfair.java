/*
 * Playfair.java
 *
 * Created on March 12, 2013, 3:14 PM 
 */
package playfair;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 *  Türk alfabesi ile plyfair şifreleme uygulaması
 * 
 *--- Varsayılan Matris ---
 *      A B C D E F
 *      G H I J K L
 *      M N O P Q R
 *      S T U V W X
 *      Y Z Ğ Ş İ Ç
 *      Ü Ö . , : ;
 * 
 * @author kyellow
 */
public class Playfair {

    private char[][] matrix;

    public Playfair(String anahtar) {

        matrix = new char[6][6];

        setKey(anahtar);
    }

    public void setKey(String anahtar) {

        if (anahtar.isEmpty())
            anahtar = "123";

        int index = 0;
        int inc = 0;    // matrisin bir boyutlu haldeki indeksi
        boolean[] has = new boolean[36];    // belirlediğim 36 karakteri için var mı kontrolü
        
        for (char c : anahtar.toUpperCase().toCharArray()) {
            
            switch (c) {
                case 'Ğ':   index = 26; break;
                case 'Ş':   index = 27; break;
                case 'İ':   index = 28; break;
                case 'Ç':   index = 29; break;
                case 'Ü':   index = 30; break;
                case 'Ö':   index = 31; break;
                case '.':   index = 32; break;
                case ',':   index = 33; break;
                case ':':   index = 34; break;
                case ';':   index = 35; break;
                default :   index = c - 'A';
            }

            if ( index < 0 || index > 35 || has[index])
                continue;
            else
                has[index] = true;
            
            inc = addCharToIndex(c,inc);
            
            if(inc > 35)
                break;
        }

        if (inc < 35)   // Tüm matris dolduruldu mu?
        {
            // Ascii de belirli olan harfleri kontrol et
            for (char c = 'A'; c <= 'Z'; c++) { 

                if (has[c - 'A']) 
                    continue;
                
                inc = addCharToIndex(c,inc);
            }
            
            // Belirlediğim diğer harfleri kontrol et
            for (int i = 26; i < 36; i++) {

                if (has[i]) 
                    continue;

                switch (i) {
                    case 26:    inc = addCharToIndex('Ğ',inc);  break;
                    case 27:    inc = addCharToIndex('Ş',inc);  break;
                    case 28:    inc = addCharToIndex('İ',inc);  break;
                    case 29:    inc = addCharToIndex('Ç',inc);  break;
                    case 30:    inc = addCharToIndex('Ü',inc);  break;
                    case 31:    inc = addCharToIndex('Ö',inc);  break;
                    case 32:    inc = addCharToIndex('.',inc);  break;
                    case 33:    inc = addCharToIndex(',',inc);  break;
                    case 34:    inc = addCharToIndex(':',inc);  break;
                    case 35:    inc = addCharToIndex(';',inc);  break;
                }
            }
        }
    } 
    
    private int addCharToIndex(char c, int i)
    {
        // Bir boyutlu matrisi 2 boyutlu kare matrise dönüştürür.
        
        if( (i % 6) > 0)
            matrix[i/6][i%6] = c;
        else
            matrix[i/6][0] = c;
                
        return ++i;
    }
    
    public void showMatrix() {
        System.out.println("--- Matrix ---");
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(" " + matrix[i][j]);
            }
            System.out.println();
        }
    }

    public char[][] getMatrix() {
        return matrix;
    }

    public String crypt(String acikMetin) {

        if (acikMetin == null) 
            return "";

        char[] digit = acikMetin.toUpperCase(new Locale("tr")).toCharArray();
        ArrayList<LetterPair> aList = new ArrayList<LetterPair>();
        boolean even = false;
        LetterPair lp = null;
        
        for (int i = 0; i < digit.length; i++) {

            if ((digit[i] < 'A' && digit[i] != '.' && digit[i] != ',' && digit[i] != ':' && digit[i] != ';') ||
                (digit[i] > 'Z' && digit[i] != 'Ğ' && digit[i] != 'Ş' && digit[i] != 'İ' && digit[i] != 'Ç' && digit[i] != 'Ü' && digit[i] != 'Ö')
               ) 
                continue;

            if (even) {

                if (lp.left == digit[i]) 
                    lp.setRight('X');
                 else 
                    lp.setRight(digit[i]);

                even = false;
                
            }else {
                
                lp = new LetterPair(digit[i], true);
                aList.add(lp);
                
                even = true;
            }
        }

        if (even) 
            lp.setRight('X');

        return aListToString(aList);
    }

    public String decrypt(String sifreliMetin) {

        if (sifreliMetin == null) 
            return "";

        char[] digit = sifreliMetin.toUpperCase(new Locale("tr")).toCharArray();
        StringBuilder sb = new StringBuilder(digit.length);
        
        for (int i = 0; i < digit.length; i++) {

            if((digit[i] >= 'A' && digit[i] <= 'Z'))
            {    
                sb.append(digit[i]);
            }
            else
            {
                switch (digit[i])
                {
                    case 'Ğ': case 'Ş': case 'İ':   
                    case 'Ç': case 'Ü': case 'Ö':   
                    case '.': case ',': case ':':   
                    case ';': 
                        sb.append(digit[i]);  break;  
                }
            }
        }

        if (sb.length() % 2 != 0) 
            return "--- Geçersiz kodlanmış mesaj ---";

        ArrayList<LetterPair> aList = new ArrayList<LetterPair>();

        digit = sb.toString().toCharArray();

        for (int i = 0; i < digit.length; i += 2) {

            LetterPair lp = new LetterPair(digit[i], false);
            lp.setRight(digit[i + 1]);
            aList.add(lp);
        }

        return aListToString(aList);
    }

    private String aListToString(ArrayList<LetterPair> aList) {

        if (aList == null || aList.isEmpty()) 
            return "";
     
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < aList.size(); i++) 
            sb.append(aList.get(i).getPair());
        
        return sb.toString();

    }

    private class LetterPair {

        private char left;
        private Point pLeft, pRight;
        private char[] digit;
        boolean coding;

        private LetterPair(char left, boolean coding) {

            this.left = left;
            this.coding = coding;
            pLeft = findPos(left);
            digit = new char[2];
        }

        private void setRight(char right) {

            pRight = findPos(right);

            if (pLeft.x == pRight.x) 
                sameRow();
            else if (pLeft.y == pRight.y) 
                sameColumn();
            else 
                diffRowCol();
            
            digit[0] = matrix[pLeft.x][pLeft.y];
            digit[1] = matrix[pRight.x][pRight.y];
        }

        private void sameRow() {
            if (coding) {

                pLeft.y = ++pLeft.y % 6;
                pRight.y = ++pRight.y % 6;
                
            } else {

                if (--pLeft.y < 0) 
                    pLeft.y = 5;
                
                if (--pRight.y < 0) 
                    pRight.y = 5;   
                
            }
        }

        private void sameColumn() {
            if (coding) {

                pLeft.x = ++pLeft.x % 6;
                pRight.x = ++pRight.x % 6;
                
            } else {

                if (--pLeft.x < 0) 
                    pLeft.x = 5;
                
                if (--pRight.x < 0) 
                    pRight.x = 5;
                
            }
        }

        private void diffRowCol() {

            int temp = pRight.y;

            pRight.y = pLeft.y;
            pLeft.y = temp;
        }

        private Point findPos(char c) {

            for (int x = 0; x < 6; x++) {
                for (int y = 0; y < 6; y++) {

                    if (matrix[x][y] == c) {
                        return new Point(x, y);
                    }
                }
            }

            throw new IllegalStateException("Harf " + c + " matrix'te bulunamadı!");
        }

        private String getPair() {
            return new String(digit);
        }
    }

    public static void main(String[] args) {

        Playfair pf = new Playfair("");
        
        System.out.println("Varsayılan Matrix : ");
        pf.showMatrix();

        pf = new Playfair("YELLOWSTORE");
        System.out.print("matrix başlangıcı \"YELLOWSTORE\"");
        System.out.println("(Not: \"L\" , \"O\" ve \"E\" yeniden yazılmaz.)");
        pf.showMatrix();

        String acikMetin = "gizli mesaj saldırın";
        System.out.println("Açık Metin : " + acikMetin);
        String sifreliMetin = pf.crypt(acikMetin);
        System.out.println("Şifreli Metin : " + sifreliMetin);

        System.out.println("Deşifreli Metin: " + pf.decrypt(sifreliMetin));

        Scanner scan = new Scanner(System.in);
        System.out.print("Anahtar Girin : ");

        String anahtar = scan.nextLine();
        pf.setKey(anahtar);
        pf.showMatrix();

        System.out.print("Mesaj Girin : ");
        String message = scan.nextLine();
        System.out.println("Açık Metin : " + message);

        sifreliMetin = pf.crypt(message);
        System.out.println("Şifrelenmiş Metin " + sifreliMetin);
        System.out.println("Deşifrelenmiş Metin : " + pf.decrypt(sifreliMetin));

    }

}
