/*
 * Copyright (C) 2013 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.*;
import java.lang.*;
import java.io.*;

/**
 * Kripto Analiz Çalışmaları ( Kasiski Metodu )
 * @author kyellow
 */
public class Kasiski {

	private static class Tekrar implements Comparable<Tekrar> {
		
        public String kelime;
        public LinkedList<Integer> konum;
	
        public Tekrar(String k, int i) {
            this.kelime = k;
            this.konum = new LinkedList<Integer>();
            this.konum.add(i);
        }

        public int compareTo(Tekrar t) {
        
            if (this.kelime.length() != t.kelime.length()){ 
                return t.kelime.length() - this.kelime.length();
            }
            else if (this.konum.size() != t.konum.size()){
                return t.konum.size() - this.konum.size();
            }
            else
                return this.kelime.compareTo(t.kelime);
        }
		
		public void addLocation(int v) {
			if( this.konum.contains(v) == false) {
				this.konum.add(v);
			}
		}
    }
    
    public static String decrypt(int mlen, String stxt) {
        
        if(stxt.isEmpty())
            return stxt;
        else
            stxt = stxt.replaceAll("[^a-zA-Z]","").toUpperCase();
        
        Tekrar t;
        String s;
        HashMap a = new HashMap<String, Tekrar>();
        Boolean has, next = true;
        
        for (int i = mlen; next; i++){
        	next = false;
        	
        	for ( int j = 0; j < stxt.length() - (i*2); j++){
        		has = false;
        		s = stxt.substring(j, j+i);
        		
        		for( int k = j + i; k < stxt.length() - i; k++){
        			
        			if( stxt.substring(k, k+i).equals(s)) {
        				next = true;
        				
        				if(has == false){
        					
        					if(a.containsKey(s) == false){
        						t = new Tekrar(s,j); 
        						a.put(s,t);
        					}
        					has = true;
        				}
        				
        				t = (Tekrar) a.get(s);
        				t.addLocation(k);
        				k += (i + 1);
        			}
        		}
        	}
        }
        
        
        
        ArrayList<Tekrar> l = new ArrayList<Tekrar>(a.values());
        Collections.sort(l);
        
        s = String.format("Uzunluk\tSayı\tKelime\tKonum\tUzaklık\n");
        s += String.format("=======\t====\t=====\t=====\t=======\n");
        
        for( Tekrar v : l){
        	s += String.format("%7d\t%4d\t%4s\t",v.kelime.length(),v.konum.size(),v.kelime);
        	
        	for( int i = 0; i < v.konum.size(); i++){
        		s += String.format("%5d\t", v.konum.get(i));
        		
        		if( i > 0){
        			s += String.format("(%d) ", v.konum.get(i) - v.konum.get(i-1));
        		}
        	}
        	
        	s += "\n";
        }
        
        return s;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
		
		int anahtar = 3;
		System.out.println("Anahtar uzunluğu : " + anahtar);
		String msg = "WERRWGCWFYVRPSPWERRIKKXGMW";
		System.out.println("Şifreli Metin : " + msg);
		System.out.println(Kasiski.decrypt(anahtar, msg));
		
        Scanner scan = new Scanner(System.in);
		System.out.println(" Minimum Anahtar Uzunluğunu Girin :"); 
        String anahtarStr = scan.nextLine();
        
        try {
            anahtar = Integer.parseInt(anahtarStr);
        }
        catch(Exception e) {
                System.out.println("Anahtarı [3,2147483647) aralığında sayı olarak girmelisin!");
        }
        
        System.out.println("Şifreli Mesajı Girin : ");
        String message = scan.nextLine();
        System.out.println("Şifreli Metin : " + message);
        
        String sonuc = Kasiski.decrypt(anahtar, message);
        System.out.println(sonuc);
    }
}
