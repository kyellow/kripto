/*
 * Copyright (C) 2013 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ceasar;

import java.util.Locale;
import java.util.Scanner;

/**
 * Türk alfabesi ile affine şifreleme uygulaması
 * @author kyellow
 */
public class AffineCipher {

    static final char[] alfabe = {'a', 'b', 'c', 'ç', 'd', 'e', 'f', 'g', 'ğ', 'h', 'ı', 'i', 'j',
        'k', 'l', 'm', 'n', 'o', 'ö', 'p', 'r', 's', 'ş', 't', 'u', 'ü', 'v', 'y', 'z'};

    private int firstKey;
    private int inverseKey;
    private int secondKey;

    public AffineCipher(int fk, int sk) {

        if (fk < 0) {
            fk *= -1;
        }

        this.firstKey = fk % 29;
        this.inverseKey = this.modinverse(29, firstKey);

        if (sk < 0) {
            sk *= -1;
        }

        this.secondKey = sk % 29;
    }

    // Modül değeri asal olduğundan (29) Fermat'nın küçük teoremi 
    public int modinverse(int asal_mod, int a) {

        int x = 1;
        int y = a;
        int z = asal_mod - 2;

        // a^b mod m    hesabı
        while (z > 0) {
            if (z % 2 == 1) {
                x *= y;
                x %= asal_mod;
            }

            y *= y;
            y %= asal_mod;
            z /= 2;
        }

        return x;
    }

    /*
     *   E(x) = (ax + b) mod m
     */
    public String crypt(String ctxt) {

        if (ctxt.isEmpty()) {
            return ctxt;
        }

        char[] clist = ctxt.toLowerCase(new Locale("tr")).toCharArray();

        for (int i = 0; i < clist.length; i++) {

            if (Character.isLetter(clist[i])) {

                for (int j = 0; j < alfabe.length; j++) {

                    if (clist[i] == alfabe[j]) {
                        clist[i] = alfabe[(j * firstKey + secondKey) % 29];
                        if(clist[i] == 'i')
                            clist[i] = 'İ';
                        else
                            clist[i] = Character.toUpperCase(clist[i]);
                        break;
                    }
                }
            }
        }

        return new String(clist);
    }

    /**
     * D(x) = a^-1 (x - b) mod m
     */
    public String decrypt(String stxt) {

        if (stxt.isEmpty()) {
            return stxt;
        }

        char[] clist = stxt.toLowerCase(new Locale("tr")).toCharArray();
        int index = 0;

        for (int i = 0; i < clist.length; i++) {

            if (Character.isLetter(clist[i])) {

                for (int j = 0; j < alfabe.length; j++) {

                    if (clist[i] == alfabe[j]) {
                        index = inverseKey * (j - secondKey + 29);
                        clist[i] = alfabe[index % 29];
                        if(clist[i] == 'i')
                            clist[i] = 'İ';
                        else
                            clist[i] = Character.toUpperCase(clist[i]);
                        break;
                    }
                }
            }
        }

        return new String(clist);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int fkey = 3;
        String alist = "abcçdefgğhıijklmnoöprsştuüvyz";
        System.out.println(" Alfabe : " + alist + " İlk Anahtar : " + fkey);

        for (int i = 0; i < 30; i++) {
            AffineCipher c = new AffineCipher(fkey, i);
            System.out.println("İlk Anahtar : " + fkey + " İkincil Anahtar : " + i + " , Alfabe : " + c.crypt(alist));
        }

        int skey = 7;
        Scanner scan = new Scanner(System.in);
        System.out.print(" İlk Anahtarı Girin [0,29): ");
        String anahtarStr = scan.nextLine();

        try {
            fkey = Integer.parseInt(anahtarStr);
        } catch (NumberFormatException e) {
            System.out.println("Anahtarı [0,29) aralığında sayı olarak girmelisin!");
        }

        System.out.print(" İkincil Anahtarı Girin [0,29): ");
        anahtarStr = scan.nextLine();
        
        try {
            skey = Integer.parseInt(anahtarStr);
        } catch (NumberFormatException e) {
            System.out.println("Anahtarı [0,29) aralığında sayı olarak girmelisin!");
        }
        
        AffineCipher cc = new AffineCipher(fkey,skey);
        System.out.println("Mesaj Girin : ");
        String message = scan.nextLine();
        System.out.println("Açık Metin : " + message);

        String sifreliMetin = cc.crypt(message);
        System.out.println("Şifrelenmiş Metin " + sifreliMetin);
        System.out.println("Deşifrelenmiş Metin : " + cc.decrypt(sifreliMetin));

    }
}
