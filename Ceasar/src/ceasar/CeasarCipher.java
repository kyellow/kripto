/*
 * Copyright (C) 2013 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ceasar;

import java.util.Locale;
import java.util.Scanner;

/**
 * Türk alfabesi ile sezar şifreleme uygulaması
 * @author kyellow
 */
public class CeasarCipher {

    static final char[] alfabe = {'a','b','c','ç','d','e','f','g','ğ','h','ı','i','j',
            'k','l','m','n','o','ö','p','r','s','ş','t','u','ü','v','y','z'};
    // 
    private int key;
    
    public CeasarCipher(int k){
    
        if(k < 0)
            k *= -1;
        
        this.key = k % 29;
    }
    
    /**
     *   E(x) = (x + b) mod m
     */
    public String crypt(String ctxt) {
        
        if(ctxt.isEmpty())
            return ctxt;
        
        char[] clist = ctxt.toLowerCase(new Locale("tr")).toCharArray();
        
        for(int i = 0; i < clist.length; i++){
        
            if(Character.isLetter(clist[i])){
                
                for(int j = 0; j < alfabe.length; j++){
                    
                    if(clist[i] == alfabe[j])
                    {
                        clist[i] = alfabe[(j+key) % 29]; 
                        if(clist[i] == 'i')
                            clist[i] = 'İ';
                        else
                            clist[i] = Character.toUpperCase(clist[i]);
                        break;
                    }
                }
            }
        }
        
        return new String(clist);
    }
    
    /**
     *   D(x) = (x - b) mod m
     */
    public String decrypt(String stxt) {
        
        if(stxt.isEmpty())
            return stxt;
        
        char[] clist = stxt.toLowerCase(new Locale("tr")).toCharArray();
        
        for(int i = 0; i < clist.length; i++){
        
            if(Character.isLetter(clist[i])){
                
                for(int j = 0; j < alfabe.length; j++){
                    
                    if(clist[i] == alfabe[j])
                    {
                        clist[i] = alfabe[ (j - key + 29) % 29]; 
                        if(clist[i] == 'i')
                            clist[i] = 'İ';
                        else
                            clist[i] = Character.toUpperCase(clist[i]);
                        break;
                    }
                }
            }
        }
        
        return new String(clist);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String alist = "abcçdefgğhıijklmnoöprsştuüvyz";
        System.out.println(" Alfabe : " + alist);

        for(int i = 0 ; i < 30; i++){
            CeasarCipher c = new CeasarCipher(i);
            System.out.println(" Anahtar : "+i+" , Alfabe : "+c.crypt(alist));
        }
     
        int anahtar = 7;
        Scanner scan = new Scanner(System.in);
        System.out.print("Anahtar Girin [0,29): ");
        String anahtarStr = scan.nextLine();
        
        try {
            anahtar = Integer.parseInt(anahtarStr);
        }
        catch(Exception e) {
                System.out.println("Anahtarı [0,29) aralığında sayı olarak girmelisin!");
        }
        
        CeasarCipher cc = new CeasarCipher(anahtar);
        System.out.println("Mesaj Girin : ");
        String message = scan.nextLine();
        System.out.println("Açık Metin : " + message);
        
        String sifreliMetin = cc.crypt(message);
        System.out.println("Şifrelenmiş Metin " + sifreliMetin);
        System.out.println("Deşifrelenmiş Metin : " + cc.decrypt(sifreliMetin));

    }

}
