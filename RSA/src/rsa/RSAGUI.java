/*
 * Copyright (C) 2013 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package rsa;

import java.awt.event.ItemEvent;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.awt.event.ItemListener;
/**
 *
 * @author kyellow
 */
public class RSAGUI extends javax.swing.JFrame {

    private final static BigInteger   one    = new BigInteger("1");
    private final static SecureRandom random = new SecureRandom();

    private ItemListener qL;
    private ItemListener pL;
    private ItemListener eL;
    
    /**
     * Creates new form RSAGUI
     */
    public RSAGUI() {
        
        initComponents();
        
        fillPrimeBox();
        
        qL = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                qboxItemStateChanged(e);
            }
        };
        
        pL = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                pboxItemStateChanged(e);
            }
        };
        
        eL = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                eboxItemStateChanged(e);
            }
        };
    }

    private void fillPrimeBox() {
        pbox.removeAllItems();
        qbox.removeAllItems();
        ebox.removeAllItems();
        
        int n_bit = Integer.parseInt(nbox.getSelectedItem().toString());

        BigInteger p = BigInteger.probablePrime(n_bit / 2, random);
        BigInteger q = BigInteger.probablePrime(n_bit / 2, random);

        for (int i = 1; i < 10; i++) {
            q = q.nextProbablePrime();
            qbox.addItem(q.toString());
            p = p.nextProbablePrime();
            pbox.addItem(p.toString());
        }

        pbox.setSelectedIndex(0);
        qbox.setSelectedIndex(5);

        calculateKeys();
        
        pbox.addItemListener(pL);
        qbox.addItemListener(qL);
        ebox.addItemListener(eL);
    }

    private void calculateKeys() {
        
        BigInteger p = new BigInteger(pbox.getSelectedItem().toString());   // seçili p değeri
        BigInteger q = new BigInteger(qbox.getSelectedItem().toString());   // seçili q değeri
              
        while(p.equals(q))
        {
            qbox.setSelectedIndex((int)(Math.random()*10));
            q = new BigInteger(qbox.getSelectedItem().toString());
        }
        
        // φN = (p-1)x(q-1)
        BigInteger phi = p.subtract(one).multiply(q.subtract(one));     //φN değeri
        ntext.setText(phi.toString());    
        
        // n = pxq
        BigInteger n = p.multiply(q);     // n değeri
        mtext.setText(n.toString());
        
        ebox.removeAllItems();
        
        int n_bit = Integer.parseInt(nbox.getSelectedItem().toString());
                
        ArrayList<BigInteger> pubkey = new ArrayList<BigInteger>();
        
        BigInteger pk = BigInteger.probablePrime(n_bit / 2, random);
        
        for(int i = 0; i < 3 ; i++)
        {
            // gcd(e,φN) = 1 , 1 < e < φN
            
            while(pk.compareTo((phi)) >= 0 || pk.gcd(phi).compareTo(BigInteger.ONE) != 0)
            {
                pk = pk.nextProbablePrime(); 
            }
            
            if(!pubkey.contains(pk))
            {
                pubkey.add(pk);
                ebox.addItem(pk.toString());
            }
            
            pk = pk.nextProbablePrime(); 
        }
        
        ebox.setSelectedIndex(0);
        
        pk = new BigInteger(ebox.getSelectedItem().toString());
        
        // d.e = 1 mod(φN) 
        BigInteger privatekey = pk.modInverse(phi);
        
        dtext.setText(privatekey.toString());
    }

    
    private String crypt(String clearText)
    {
        BigInteger msg = new BigInteger(clearText.getBytes());
        
        BigInteger pk = new BigInteger(ebox.getSelectedItem().toString());
        BigInteger n  = new BigInteger(mtext.getText());
        
        if(msg.compareTo(n) < 0 ) // [0,n)
            return msg.modPow(pk, n).toString(16);
        else
            return "Mesaj uzunluğu modül değerinden(n = pxq) küçük olmalıdır.\n N-Bit değerini büyütmelisin!";
    }
    
    private String decrypt(String codedText)
    {
        BigInteger cmsg = new BigInteger(codedText,16);
        
        BigInteger sk = new BigInteger(dtext.getText());
        BigInteger n  = new BigInteger(mtext.getText());
        
        if(cmsg.compareTo(n) < 0 ) // [0,n)
            return new String(cmsg.modPow(sk, n).toByteArray());
        else
            return "Şifreli mesaj uzunluğu modül değerinden(n = pxq) küçük olmalıdır.\n N-Bit değerini büyütmelisin!";
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        qbox = new javax.swing.JComboBox<>();
        nbox = new javax.swing.JComboBox<>();
        pbox = new javax.swing.JComboBox<>();
        ebox = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ntext = new javax.swing.JTextField();
        dtext = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        codedText = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        clearText = new javax.swing.JTextArea();
        mtext = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("RSA Uygulaması");
        setMaximumSize(new java.awt.Dimension(800, 800));
        setResizable(false);

        jLabel1.setText("p Asal Sayısı");

        jLabel2.setText("N-Bit Değeri");

        jLabel3.setText("φN");

        jLabel4.setText("q Asal Sayısı");

        nbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "8", "16", "32", "64", "128", "256", "512", "1024", "2048", "4096" }));
        nbox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nboxItemStateChanged(evt);
            }
        });

        jLabel5.setText("e Değeri");

        jLabel6.setText("n");

        ntext.setEditable(false);

        dtext.setEditable(false);

        codedText.setColumns(20);
        codedText.setRows(5);
        codedText.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Şifreli Mesaj", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Liberation Sans", 0, 12), java.awt.Color.blue)); // NOI18N
        codedText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                codedTextKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(codedText);

        clearText.setColumns(20);
        clearText.setRows(5);
        clearText.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Açık Mesaj", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Liberation Sans", 0, 12), java.awt.Color.blue)); // NOI18N
        clearText.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                clearTextKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(clearText);

        mtext.setEditable(false);

        jLabel8.setText("d");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(qbox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nbox, javax.swing.GroupLayout.Alignment.TRAILING, 0, 495, Short.MAX_VALUE)
                            .addComponent(pbox, javax.swing.GroupLayout.Alignment.TRAILING, 0, 495, Short.MAX_VALUE)
                            .addComponent(ebox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ntext))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mtext))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dtext)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(qbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ebox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ntext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_nboxItemStateChanged
        // TODO add your handling code here:
        qbox.removeItemListener(qL);
        pbox.removeItemListener(pL);
        ebox.removeItemListener(eL);
        
        fillPrimeBox();
    }//GEN-LAST:event_nboxItemStateChanged

    private void clearTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_clearTextKeyReleased
        // TODO add your handling code here:
        String line = clearText.getText();
        
        if(!line.isEmpty())
            codedText.setText(crypt(line));
        else
            codedText.setText(line);
    }//GEN-LAST:event_clearTextKeyReleased

    private void codedTextKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codedTextKeyReleased
        // TODO add your handling code here:
        String line = codedText.getText();
        
        if(!line.isEmpty())
            clearText.setText(decrypt(line));
        else
            clearText.setText(line);
    }//GEN-LAST:event_codedTextKeyReleased

     private void eboxItemStateChanged(java.awt.event.ItemEvent evt) {                                      
        // TODO add your handling code here:
        BigInteger pk = new BigInteger(ebox.getSelectedItem().toString());
        
        // d.e = 1 mod(φN) 
        BigInteger privatekey = pk.modInverse(new BigInteger(ntext.getText()));
        
        dtext.setText(privatekey.toString());
    }
    
    private void qboxItemStateChanged(java.awt.event.ItemEvent evt) {                                      
        // TODO add your handling code here:
        calculateKeys();
    }
    
    private void pboxItemStateChanged(java.awt.event.ItemEvent evt) {                                      
        // TODO add your handling code here:
        calculateKeys();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RSAGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RSAGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RSAGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RSAGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new RSAGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea clearText;
    private javax.swing.JTextArea codedText;
    private javax.swing.JTextField dtext;
    private javax.swing.JComboBox<String> ebox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField mtext;
    private javax.swing.JComboBox<String> nbox;
    private javax.swing.JTextField ntext;
    private javax.swing.JComboBox<String> pbox;
    private javax.swing.JComboBox<String> qbox;
    // End of variables declaration//GEN-END:variables
}
